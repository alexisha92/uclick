package kr.co.uclick.service;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import kr.co.uclick.configuration.SpringConfiguration;
import kr.co.uclick.entity.User;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SpringConfiguration.class)
public class UclickServiceTest {
	
	@Autowired
	UserService userService;
	
	@Autowired
	PhoneService phoneService;
	
//	@Test
//	public void findAllPhone() {
//		//User u = new User();
//		//u.setName("Chloe");
//		//userRepository.deleteById(3L);
//		//phoneRepository.deleteById(5L);
//		//userService.updatePrcss("Manon", 2L);
//	List<Phone> phones = phoneService.phoneList();
//		System.out.println(phones.size());
//		
//		for (Phone p : phones) {
//
//			System.out.println("[User Name]: " + p.getUser().getName() + " [Phone ID]: " + p.getId() + " [PHONE NUMBER]: " + p.getNum());
//		}
//
//	}
	@Ignore
	@Test
	@Transactional
	public void search() {
		List<User> u = userService.findAllSearchByName("Eliott");
		for (User user : u) {
			System.out.println("[user id] : " + user.getId() + "[user name] : " +
				user.getName() + " [phone] : " + user.getPhones().stream().collect(Collectors.toList()).get(0).getNum());	
			}
	}
	
	@Test
	public void test() {
		String num1="가나다";
		for(int i =1; i<2 ; i++) {
			if(("num"+i).equals("")) {
				System.out.println("공백");
			}else {
				System.out.println(num1);
			}
		}
		
	}
}
