//package kr.co.uclick.repository;
//
//import javax.jdo.annotations.Transactional;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import kr.co.uclick.configuration.SpringConfiguration;
//import kr.co.uclick.entity.Phone;
//import kr.co.uclick.entity.User;
//
//@RunWith(SpringRunner.class)
//@ContextConfiguration(classes = SpringConfiguration.class)
//public class UclickRepositoryTest {
//	@Autowired
//	private PhoneRepository phoneRepository;
//	@Autowired
//	private UserRepository userRepository;
//	
//	@Test
//	@Transactional
//	public void oneToMany_OneWay() {
//		User name = new User("Eliott");
//		
//		User name2 = new User("Marina");
//		
//		User name3 = new User("Alejandro");
//		
//		userRepository.save(name);
//		userRepository.save(name2);
//		userRepository.save(name3);
//		
//		Phone ph = new Phone();
//		Phone ph2 = new Phone();
//		Phone ph3 = new Phone();
//		
//		ph.setUser(name);
//		ph.setNum("010-1234-5678");
//		phoneRepository.save(ph);
//		
//		ph2.setUser(name2);
//		ph2.setNum("010-9876-5432");
//		phoneRepository.save(ph2);
//		
//		ph3.setUser(name3);
//		ph3.setNum("010-1111-2222");
//		phoneRepository.save(ph3);
//		
//		Phone ph4 = new Phone();
//		
//		ph4.setUser(name);
//		ph4.setNum("010-2222-3333");
//		phoneRepository.save(ph4);
//		
//	}
//
//}
