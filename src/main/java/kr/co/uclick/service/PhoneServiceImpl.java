package kr.co.uclick.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import kr.co.uclick.entity.Phone;
import kr.co.uclick.entity.User;
import kr.co.uclick.repository.PhoneRepository;
import kr.co.uclick.repository.UserRepository;

@Service
public class PhoneServiceImpl implements PhoneService {
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PhoneRepository phoneRepository;
	
	//번호 추가	
	@Override
	public void addPhone(Long id, String num) {
		User u = userRepository.findById(id).get();
		Phone p = new Phone();
		
		p.setUser(u);
		p.setNum(num);
		
		phoneRepository.save(p);
	}
	
	
	//번호 삭제
	@Override
	public void deletePhone(Long id) {
		phoneRepository.deleteById(id);
		
	}
	
	//번호 검색
	@Override
	public List<Phone> findAllSearchByNum(String num) {
		return phoneRepository.findAllSearchByNum(num);
	}
}
