package kr.co.uclick.service;

import java.util.List;

import kr.co.uclick.entity.User;

public interface UserService {
	
	public List<User> userList(); 		               																 //사용자 목록
	public void insert(String name, String level, String num1, String num2, String num3, String num4, String num5);  //사용자 입력 저장
    public User oneView(Long id);          	      	   																 //사용자 상세화면
    public User updatePage(Long id);					   															 //사용자 수정 페이지
    public void update(String name, String level, String num, Long id, Long phoneId);     							 //사용자 수정 저장
    public void deleteUser(Long id);                   																 //사용자 삭제
    public List<User> findAllSearchByName(String name); 														     //사용자 이름 검색
}
