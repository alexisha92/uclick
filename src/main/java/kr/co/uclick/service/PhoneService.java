package kr.co.uclick.service;

import java.util.List;

import kr.co.uclick.entity.Phone;

public interface PhoneService {
	
	public void addPhone(Long id, String num); 			//휴대폰 추가
    public void deletePhone(Long id);            	    //휴대폰 삭제
    public List<Phone> findAllSearchByNum(String num);  //휴대폰 번호 검색
}
