package kr.co.uclick.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.uclick.entity.Phone;
import kr.co.uclick.entity.User;
import kr.co.uclick.repository.PhoneRepository;
import kr.co.uclick.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PhoneRepository phoneRepository;
    
	//목록
	@Override
	public List<User> userList() {
		return userRepository.findAllByOrderByIdDesc();
	}
    
	//사용자 입력 저장
	@Override
	public void insert(String name, String level, String num1, String num2, String num3, String num4, String num5) {
		
		User u = new User(name, level);
		Phone p = new Phone();
		
		userRepository.save(u);
		p.setUser(u);
		p.setNum(num1);
		phoneRepository.save(p);
		
		if(!num2.equals(null) && !num2.equals("")) {
			Phone p2 = new Phone();
			p2.setUser(u);
			p2.setNum(num2);
			phoneRepository.save(p2);
		}
		
		if(!num3.equals(null) && !num3.equals("")) {
			Phone p3 = new Phone();
			p3.setUser(u);
			p3.setNum(num3);
			phoneRepository.save(p3);
		}
		
		if(!num4.equals(null) && !num4.equals("")) {
			Phone p4 = new Phone();
			p4.setUser(u);
			p4.setNum(num4);
			phoneRepository.save(p4);
		}
		
		if(!num5.equals(null) && !num5.equals("")) {
			Phone p5 = new Phone();
			p5.setUser(u);
			p5.setNum(num5);
			phoneRepository.save(p5);
		}
	}
	
	//사용자 상세화면
	@Override
	public User oneView(Long id) {
		return userRepository.getOne(id);
	}
	
	//사용자 수정 페이지
	@Override
	public User updatePage(Long id) {
		return userRepository.getOne(id);
	}
	
	//사용자 수정 저장
	@Override
	public void update(String name, String level, String num, Long id, Long phoneId) {
//		User u = userRepository.getOne(id);
		User u = userRepository.findById(id).get();       //사용자 ID
		Phone p = phoneRepository.findById(phoneId).get();//번호 ID
		
		u.setName(name);
		u.setLevel(level);
		userRepository.save(u);
		p.setNum(num);
		phoneRepository.save(p);
	}
	
	//목록에서 사용자 삭제
	@Override
	public void deleteUser(Long id) {
		userRepository.deleteById(id);
	}
	
	//사용자 검색
	@Override
	public List<User> findAllSearchByName(String name) {
		return userRepository.findAllSearchByName(name);
	}
}
