package kr.co.uclick.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import kr.co.uclick.entity.Phone;
import kr.co.uclick.entity.User;
import kr.co.uclick.service.PhoneService;
import kr.co.uclick.service.UserService;

@Controller
@Transactional
public class UclickController {

	@Autowired
	private UserService userService;

	@Autowired
	private PhoneService phoneService;

	// 게시글 목록
	@RequestMapping(value = "findAll.html")
	public String userList(@RequestParam HashMap<String, String> map, Model model) {

		// 검색관련 기능
		String option = map.get("option"); // 사용자 또는 전화번호 셀렉트
		String word = map.get("word"); // 입력어
		boolean search = false; // false 미검색, true 검색
		if (option == null || option.equals("")) {
			option = "false";
			search = false;
		}

		if (word == null || word.equals("")) {
			word = "false";
			search = false;
		}

		List<Phone> phoneList = new ArrayList<Phone>();
		List<User> userList = new ArrayList<User>();
		List<User> userList2 = new ArrayList<User>();

		// 미검색일 때
		if (option.equals("false") || word.equals("false")) {
			search = false;
			userList = userService.userList(); // 리스트 변수에 결과값을 담는다.
			// 하이버네이트에서 매핑객체를 가져올 때 일대다, 다대일은 lazyfetch가 기본 설정.
			// 따라서 부모객체가 자식객체를 갖고올 때는 프락시 객체만 갖고 있다.
			// 부모 객체를 가져오고 세션이 닫힌 뒤, 자식 객체를 콜하면 예외가 발생하기에 hibernate.initialize로
			// 필요 객체만 미리 initialize 상태로 가져올 수 있다.
		} else {
			// 검색일 때
			search = true;
			if (option.equals("name")) { // 이름으로 검색
				userList = userService.findAllSearchByName(word);

			} else if (option.equals("number")) { // 번호로 검색
				phoneList = phoneService.findAllSearchByNum(word);

				for (int i = 0; i < phoneList.size(); i++) {
					userList2.add(phoneList.get(i).getUser());
				}
//				for(int i = 0; i<userList2.size(); i++) {
//					if(!userList.contains(userList2.get(i)))
//						userList.add(userList2.get(i));
//				}

				// HashSet을 사용하여 중복제거
				HashSet<User> check = new HashSet<User>(userList2); // HashSet에 arr데이터 삽입
				userList = new ArrayList<User>(check); // 중복이 제거된 HashSet을 다시 ArrayList에 삽입
			}
		}
		for (User user : userList) {
			Hibernate.initialize(user.getPhones());
		}

		model.addAttribute("phoneList", phoneList);
		model.addAttribute("userList", userList);
		model.addAttribute("search", search);
		model.addAttribute("option", option);
		model.addAttribute("word", word);

		return "findAll";
	}

	// 사용자 상세 페이지
	@RequestMapping(value = "oneView.html")
	public String oneView(@RequestParam HashMap<String, String> map, Model model) {
		long id = Long.parseLong(map.get("id"));
		userService.oneView(id);

		User userView = userService.oneView(id);
		model.addAttribute("userView", userView);

		Hibernate.initialize(userView.getPhones());

		return "oneView";
	}

	// 새로운 사용자 데이터 입력 페이지
	@RequestMapping(value = "insert.html")
	public String insert(@RequestParam HashMap<String, String> map, Model model) {
		return "insert";
	}

	// 입력 데이터 저장 페이지
	@RequestMapping(value = "insertDone.html")
	public String insertDone(@RequestParam HashMap<String, String> map, Model model) {
		String name = map.get("name");
		String level = map.get("level");
		String num1 = map.get("num1");
		String num2 = map.get("num2");
		String num3 = map.get("num3");
		String num4 = map.get("num4");
		String num5 = map.get("num5");
		
		userService.insert(name, level, num1, num2, num3, num4, num5); // 사용자 이름과 메인번호 저장

		return "insertDone";
	}

	// 목록에서 체크박스로 사용자 데이터 삭제
	@RequestMapping(value = "deleteUser.html")
	public String deleteUser(@RequestParam("deleteUser") String[] delUser, ModelMap modelMap) {
		for (String id : delUser) {
			long del = Long.parseLong(id);
			userService.deleteUser(del);
		}
		return "deleteUser";
	}

	// 상세 화면에서 사용자 삭제
	@RequestMapping(value = "deleteThis.html")
	public String deleteThis(@RequestParam HashMap<String, String> map, Model model) {
		long id = Long.parseLong(map.get("id"));
		userService.deleteUser(id);

		return "deleteUser";
	}

	// 사용자 데이터 수정 페이지
	@RequestMapping(value = "update.html")
	public String updatePage(@RequestParam HashMap<String, String> map, Model model) {
		long id = Long.parseLong(map.get("id"));
			
		User userEdit = userService.updatePage(id);
		model.addAttribute("userEdit", userEdit);
		userService.updatePage(id);

		Hibernate.initialize(userEdit.getPhones());
		return "update";
	}

	// 사용자 데이터 수정
	@RequestMapping(value = "updateDone.html")
	public String update(@RequestParam HashMap<String, String> map, Model model) {
		String name = map.get("name"); // 사용자 이름
		String level = map.get("level"); // 사용자 등급
		long id = Long.parseLong(map.get("id")); // 사용자 아이디
		long total = Long.parseLong(map.get("total")); // 사용자 전화 개수

		ArrayList<String> numbers = new ArrayList<String>();
		ArrayList<Long> phoneIds = new ArrayList<Long>();

		for (int i = 0; i < total; i++) { // 두 ArrayList에 사용자의 전화 각각의 아이디와 번호를 넣는다.
			numbers.add(map.get("number" + i));
			phoneIds.add(Long.parseLong(map.get("phoneId" + i)));

			userService.update(name, level, numbers.get(i), id, phoneIds.get(i));
		}

		User userView = userService.oneView(id); // 수정 완료 후 해당 데이터 상세 페이지로 이동하기 위해
		model.addAttribute("userView", userView);// 모델에 어트리뷰트

		return "updateDone";
	}

	// 사용자 개별 변호 삭제
	@RequestMapping(value = "deletePhone.html")
	public String deletePhone(@RequestParam HashMap<String, String> map, Model model) {
		long id = Long.parseLong(map.get("id")); // 전화 아이디
		long user_id = Long.parseLong(map.get("user_id")); // 사용자 아이디

		phoneService.deletePhone(id);

		User userView = userService.oneView(user_id); // 번호 삭제 후 해당 데이터 상세 페이지로 이동하기 위해
		model.addAttribute("userView", userView); // 모델에 어트리뷰트

		return "deletePhone";
	}

	// 사용자 번호 추가 페이지
	@RequestMapping(value = "addPhone.html")
	public String addPhone(@RequestParam HashMap<String, String> map, Model model) {
		long id = Long.parseLong(map.get("id")); // 사용자 아이디
		//userService.oneView(id);

		User userView = userService.oneView(id);
		model.addAttribute("userView", userView);

		Hibernate.initialize(userView.getPhones());

		return "addPhone";
	}

	@RequestMapping(value = "addPhoneDone.html")
	public String addPhoneDone(@RequestParam HashMap<String, String> map, Model model) {
		long id = Long.parseLong(map.get("id")); // 사용자 아이디
		String num = map.get("num");
        
		phoneService.addPhone(id, num);
		
		User userView = userService.oneView(id);
		model.addAttribute("userView", userView);

		return "addPhoneDone";
	}
}
