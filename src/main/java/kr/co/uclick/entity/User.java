package kr.co.uclick.entity;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.cache.annotation.Cacheable;


@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Entity
public class User {
	@Id
	@GeneratedValue
	@Column
	private Long id;
	
	@Column(length = 20) //이름 20바이트로 제한
	private String name;
	
	@Column
	private String level;
	
	public User() {}
	
	public User(String name, String level) {
		this.name = name;
		this.level = level;
	}
	
	@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONE)
	@OneToMany(cascade=CascadeType.ALL, mappedBy="user")
	private Collection<Phone> phones;

	//Getter & Setter 
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}
	
	public Collection<Phone> getPhones() {
		return phones;
	}

	public void setPhones(Collection<Phone> phones) {
		this.phones = phones;
	}
}
