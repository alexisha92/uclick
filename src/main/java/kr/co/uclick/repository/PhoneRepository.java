package kr.co.uclick.repository;

import java.util.List;

import javax.persistence.QueryHint;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import kr.co.uclick.entity.Phone;
import kr.co.uclick.entity.User;

@Repository
public interface PhoneRepository extends JpaRepository<Phone, Long> {
	//QueryCache 적용
		@QueryHints(value= {
				@QueryHint(name="org.hibernate.cacheable", value="true"),
				@QueryHint(name="org.hibernate.cacheMode", value="NORMAL")
		})
	
	@Query("select t from Phone t where num like concat('%',:searchString,'%')")
	List<Phone> findAllSearchByNum(@Param("searchString") String searchString);
	
//	@Query("SELECT CASE WHEN count(e) > 0 THEN true ELSE false END FROM Phone e where e.num = ?1")
//	public boolean existsByNumber(String num);
}
