package kr.co.uclick.repository;

import java.util.List;

import javax.persistence.QueryHint;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import kr.co.uclick.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	//QueryCache 적용
	@QueryHints(value= {
			@QueryHint(name="org.hibernate.cacheable", value="true"),
			@QueryHint(name="org.hibernate.cacheMode", value="NORMAL")
			//@QueryHint(name="org.hibernate.cacheRegion", value="user-search-like"),

	})
	
	List<User> findAllByOrderByIdDesc();
	
	@Query("select t from User t where name like concat('%',:searchString,'%')")
	List<User> findAllSearchByName(@Param("searchString") String searchString);

	
}
