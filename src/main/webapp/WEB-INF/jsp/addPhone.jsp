<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>상세 데이터</title>
<script>
function digit_check(event) { //숫자만 받는 함수 설정
	event = event || window.event;
	var keyID = (event.which) ? event.which : event.keyCode;
	if(keyID < 48 || keyID > 57){
		event.target.value = event.target.value.replace(/[^0-9]/g, "");
	}
}
</script>
</head>
<body>
	<div class="page-wrapper">
		<div class="container-fluid">
			<div class="col-lg-8">
				<div class="col-lg-12">
					<h1 class="page-header">사용자 데이터</h1>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<button class="btn btn-outline btn-primary pull-right"
							onclick="location.href='oneView.html?id=${userView.id}'">취소</button>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">사용자 상세 데이터</div>
					<div class="panel-body">
						<form action="addPhoneDone.html" method="post"
							style="display: inline">
							<!-- /슬러시가 있으면 절대경로가 된다. -->
							<input type="hidden" name="id" id="id" value="${userView.id}">
							<div class="row form-group">
								<label class="col-lg-2">ID</label>
								<div class="col-lg-8">${userView.id}</div>
							</div>
							<div class="row form-group">
								<label class="col-lg-2">이름</label>
								<div class="col-lg-8">${userView.name}</div>
							</div>
							<div class="row form-group">
								<label class="col-lg-2">등급</label>
								<div class="col-lg-8">${userView.level}</div>
							</div>
							<c:forEach items="${userView.phones}" var="u" varStatus="status">
								<div class="row form-group">
									<label class="col-lg-2">휴대전화 ${status.count} </label>
									<div class="col-lg-8">${fn:substring(u.num, 0, 3)}-${fn:substring(u.num, 3, 7)}-${fn:substring(u.num, 7, 11)}
									</div>
								</div>
							</c:forEach>
							<div class="row form-group">
								<label class="col-lg-2">휴대전화 추가</label>
								<div class="col-lg-8">
									<input 
										type="text" onkeyup="return digit_check(event)"
										class="form-control" id="num" name="num" value=""
										placeholder="숫자만 입력" required>
								</div>
							</div>
							<button type="submit" class="btn btn-outline btn-info pull-right">저장</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>