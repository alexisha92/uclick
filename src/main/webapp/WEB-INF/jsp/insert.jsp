<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>insert</title>
<script>
	function remover(event) { //한글과 영어가 아니라면 지워지는 함수 설정
		event = event || window.event;
		var keyID = (event.which) ? event.which : event.keyCode;
		if (keyID == 8 || keyID == 46 || keyID == 37 || keyID == 32 || keyID == 39)
			return;
		else
			event.target.value = event.target.value.replace(/[^(ㄱ-힣\sa-zA-Z\s)]/gi, "");
	}

	function digit_check(event) { //숫자만 받는 함수 설정
		event = event || window.event;
		var keyID = (event.which) ? event.which : event.keyCode;
    	if(keyID < 48 || keyID > 57){
    		event.target.value = event.target.value.replace(/[^0-9]/g, "");
    	}
	}

	function chkword(obj, maxByte) { //20바이트 이상 문자열 알람 및 초과 문자열 자르는 함수  
        var strValue = obj.value;
        var strLen = strValue.length;
        var totalByte = 0;
        var len = 0;
        var oneChar = "";
        var str2 = "";
        for (var i = 0; i < strLen; i++) {
            oneChar = strValue.charAt(i);
            if (escape(oneChar).length > 4) {
                totalByte += 2;
            } else {
                totalByte++;
            }
            if (totalByte <= maxByte) {//입력한 문자 길이보다 넘치면 잘라내기 위해 저장
                len = i + 1;
            }
        }
        if (totalByte > maxByte) {//넘어가는 글자는 자른다.
            alert(maxByte + "자를 초과 입력 할 수 없습니다.");
            str2 = strValue.substr(0, len);
            obj.value = str2;
            chkword(obj, 4000);
        }
    }
</script>
</head>
<body>
	<div class="page-wrapper">
		<div class="container-fluid">
			<div class="col-lg-8">
				<!--게시판 넓이 -->
				<div class="col-lg-12">
					<h1 class="page-header">사용자 데이터 입력</h1>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<button class="btn btn-outline btn-primary pull-right"
							onclick="location.href='findAll.html'">목록</button>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">새로운 사용자</div>
					<div class="panel-body">
						<form action="insertDone.html" method="post"
							style="display: inline">
							<!-- /슬러시가 있으면 절대경로가 된다. -->
							<div class="row form-group">
								<label class="col-lg-2">이름</label>
								<div class="col-lg-8">
									<input 
										type="text" onkeydown="chkword(this, 20)"
										onkeyup='remover(event)'
										class="form-control" id="name" name="name"
										placeholder="한글 또는 알파벳 입력" required>
								</div>
							</div>
							<div class="row form-group">
								<label class="col-lg-2">등급</label>
								<div class="col-lg-8">
									<input type="radio" name="level" value="SILVER" checked>SILVER<br>
									<input type="radio" name="level" value="GOLD">GOLD<br>
									<input type="radio" name="level" value="VIP">VIP<br>
								</div>
							</div>
							<div class="row form-group">
								<label class="col-lg-2">휴대전화</label>
								<div class="col-lg-8">
									<input 
									    type="text" onkeyup="return digit_check(event)"
										class="form-control" id="num1" name="num1" required
										placeholder="메인 전화번호 필수 입력 (숫자만 입력)"> 
									<input
										type="text" onkeyup="return digit_check(event)"
										class="form-control" id="num2" name="num2"
										placeholder="추가 전화번호 2"> 
									<input 
										type="text" onkeyup="return digit_check(event)" 
										class="form-control"id="num3" name="num3" 
										placeholder="추가 전화번호 3"> 
									<input
										type="text" onkeyup="return digit_check(event)"
										class="form-control" id="num4" name="num4"
										placeholder="추가 전화번호 4"> 
									<input 
									    type="text" onkeyup="return digit_check(event)" 
									    class="form-control"id="num5" name="num5" 
									    placeholder="추가 전화번호 5">
								</div>
							</div>
							<button type="submit" class="btn btn-outline btn-info pull-right">저장</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>