<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>목록</title>
<script>
	function check() {
		var obj = document.getElementsByClassName('deleteUser');
		var obj2 = document.ch;
		var l = obj.length;
		var count = 0;
		for (i = 0; i < l; i++) {
			//console.log(obj.item(i));
			//console.log(obj.item(i).checked);
			if (obj.item(i).checked) {
				count += 1;
			}
		}
		//console.log(count);
		if (count == 0) {
			alert("선택된 사용자가 없습니다.");
		} else {
			obj2.submit();
		}
	}
</script>
</head>
<body>
	<div class="page-wrapper">
		<div class="container-fluid">
			<div class="col-lg-8">
				<div class="col-lg-12">
					<h1 class="page-header">사용자 데이터</h1>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<form action="findAll.html" method="post" style="display: inline">
							<select name="option">
								<option value="name">이름</option>
								<option value="number">번호</option>
							</select> <input type="search" name="word">
							<button type="submit" class="btn btn-outline btn-primary">
								<i class="fa fa-edit fa-fw"></i> 검색
							</button>
						</form>
					</div>
					<div class="col-lg-12">
						<button type="button"
							class="btn btn-outline btn-primary pull-right"
							onclick="location.href='insert.html'">
							<i class="fa fa-edit fa-fw"></i> 추가
						</button>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">목록</div>
					<div class="panel-body">
						<form method="post" action="deleteUser.html" name="ch">
							<table class="table table-hover">
								<thead>
									<tr>
										<th>ID</th>
										<th>이름</th>
										<th>휴대전화</th>
										<th>등급</th>
										<th>편집</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${userList}" var="u">
										<tr>
											<td><input class="deleteUser" type="checkbox"
												name="deleteUser" id="deleteUser" value="${u.id}" />${u.id}</td>
											<td><a href='oneView.html?id=${u.id}'>${u.name}</a></td>
											<td>${fn:substring(u.phones[0].num, 0, 3)}-${fn:substring(u.phones[0].num, 3, 7)}-${fn:substring(u.phones[0].num, 7, 11)}
												<c:if test="${fn:length(u.phones) gt 1}">
													<span style="font-weight: bold; color: #0099ff;">+${fn:length(u.phones)-1}
														more</span>
												</c:if>
											</td>
											<td>${u.level}</td>
											<td>
												<button type="button"
													class="btn btn-outline btn-primary pull-center"
													onclick="location.href='update.html?id=${u.id}'">
													<i class="fa fa-edit fa-fw"></i>수정
												</button>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</form>
						<button onclick="check();" class="btn btn-outline btn-info">삭제</button>
					</div>
				</div>

			</div>
		</div>
	</div>

</body>
</html>
